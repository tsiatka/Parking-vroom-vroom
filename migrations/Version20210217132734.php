<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210217132734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE parking (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, nb_places INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE place (id INT AUTO_INCREMENT NOT NULL, parking_id INT NOT NULL, number VARCHAR(255) NOT NULL, is_used TINYINT(1) NOT NULL, INDEX IDX_741D53CDF17B2DD (parking_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rent (id INT AUTO_INCREMENT NOT NULL, place_id INT NOT NULL, used_by_id INT NOT NULL, started_at DATETIME NOT NULL, finished_at DATETIME NOT NULL, INDEX IDX_2784DCCDA6A219 (place_id), INDEX IDX_2784DCC4C2B72A8 (used_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE residence (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE residence_parking (residence_id INT NOT NULL, parking_id INT NOT NULL, INDEX IDX_FCB53E3C8B225FBD (residence_id), INDEX IDX_FCB53E3CF17B2DD (parking_id), PRIMARY KEY(residence_id, parking_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE residence_user (residence_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_4E41D76D8B225FBD (residence_id), INDEX IDX_4E41D76DA76ED395 (user_id), PRIMARY KEY(residence_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, last_name VARCHAR(255) NOT NULL, telephone VARCHAR(255) DEFAULT NULL, nb_appartement VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_place (user_id INT NOT NULL, place_id INT NOT NULL, INDEX IDX_96DFA895A76ED395 (user_id), INDEX IDX_96DFA895DA6A219 (place_id), PRIMARY KEY(user_id, place_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE place ADD CONSTRAINT FK_741D53CDF17B2DD FOREIGN KEY (parking_id) REFERENCES parking (id)');
        $this->addSql('ALTER TABLE rent ADD CONSTRAINT FK_2784DCCDA6A219 FOREIGN KEY (place_id) REFERENCES place (id)');
        $this->addSql('ALTER TABLE rent ADD CONSTRAINT FK_2784DCC4C2B72A8 FOREIGN KEY (used_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE residence_parking ADD CONSTRAINT FK_FCB53E3C8B225FBD FOREIGN KEY (residence_id) REFERENCES residence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE residence_parking ADD CONSTRAINT FK_FCB53E3CF17B2DD FOREIGN KEY (parking_id) REFERENCES parking (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE residence_user ADD CONSTRAINT FK_4E41D76D8B225FBD FOREIGN KEY (residence_id) REFERENCES residence (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE residence_user ADD CONSTRAINT FK_4E41D76DA76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_place ADD CONSTRAINT FK_96DFA895A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_place ADD CONSTRAINT FK_96DFA895DA6A219 FOREIGN KEY (place_id) REFERENCES place (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE place DROP FOREIGN KEY FK_741D53CDF17B2DD');
        $this->addSql('ALTER TABLE residence_parking DROP FOREIGN KEY FK_FCB53E3CF17B2DD');
        $this->addSql('ALTER TABLE rent DROP FOREIGN KEY FK_2784DCCDA6A219');
        $this->addSql('ALTER TABLE user_place DROP FOREIGN KEY FK_96DFA895DA6A219');
        $this->addSql('ALTER TABLE residence_parking DROP FOREIGN KEY FK_FCB53E3C8B225FBD');
        $this->addSql('ALTER TABLE residence_user DROP FOREIGN KEY FK_4E41D76D8B225FBD');
        $this->addSql('ALTER TABLE rent DROP FOREIGN KEY FK_2784DCC4C2B72A8');
        $this->addSql('ALTER TABLE residence_user DROP FOREIGN KEY FK_4E41D76DA76ED395');
        $this->addSql('ALTER TABLE user_place DROP FOREIGN KEY FK_96DFA895A76ED395');
        $this->addSql('DROP TABLE parking');
        $this->addSql('DROP TABLE place');
        $this->addSql('DROP TABLE rent');
        $this->addSql('DROP TABLE residence');
        $this->addSql('DROP TABLE residence_parking');
        $this->addSql('DROP TABLE residence_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_place');
    }
}
