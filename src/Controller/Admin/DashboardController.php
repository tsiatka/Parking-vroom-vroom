<?php

namespace App\Controller\Admin;

use App\Entity\Parking;
use App\Entity\Place;
use App\Entity\Rent;
use App\Entity\Residence;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setFaviconPath('favicon-32x32.png')
            ->setTitle('Parking Symfony');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Residences', 'fas fa-building', Residence::class);
        yield MenuItem::linkToCrud('Parkings', 'fas fa-parking', Parking::class);
        yield MenuItem::linkToCrud('Places', 'fas fa-car', Place::class);
        yield MenuItem::linkToCrud('Locations', 'fas fa-handshake', Rent::class);
    }
}
