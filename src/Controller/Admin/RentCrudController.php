<?php

namespace App\Controller\Admin;

use App\Entity\Rent;
use App\Repository\PlaceRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class RentCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Rent::class;
    }


    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Location')
            ->setEntityLabelInPlural('Locations');
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = [
            AssociationField::new('place', 'Place louée')->setFormTypeOptions(['query_builder' => function (PlaceRepository  $placeRepository) {
                return $placeRepository->createQueryBuilder('place')
                    ->where('place.isUsed LIKE :isUsed')->setParameter('isUsed', true);
            }]),
            AssociationField::new('usedBy', 'Louée par'),
            DateTimeField::new('startedAt', 'Début de la location'),
            DateTimeField::new('finishedAt', 'Fin de la location'),
        ];

        return $fields;
    }
}
