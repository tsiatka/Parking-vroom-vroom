<?php

namespace App\Controller\Admin;

use App\Entity\Place;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class PlaceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Place::class;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_INDEX, Action::DETAIL);
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('users', 'Propriétaire'))
            ->add('parking');
    }

    public function configureFields(string $pageName): iterable
    {
        $fields = [
            TextField::new('number', 'Numéro / nom de la place'),
            AssociationField::new('users', 'Appartient à')->setFormTypeOptions(['by_reference' => false])->formatValue(function ($value, $entity) {
                $str = $entity->getUsers()[0];
                for ($i = 1; $i < $entity->getUsers()->count(); $i++) {
                    $str = $str . ", " . $entity->getUsers()[$i];
                }
                return $str;
            }),
            AssociationField::new('parking', 'Est située dans'),
            BooleanField::new('isUsed', 'Disponible')->setFormTypeOptionIfNotSet('label_attr.class', 'switch-custom'),
        ];

        return $fields;
    }
}
