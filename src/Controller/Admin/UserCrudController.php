<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TelephoneField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserCrudController extends AbstractCrudController
{

  public static function getEntityFqcn(): string
  {
    return User::class;
  }

  public function configureActions(Actions $actions): Actions
  {
    return $actions
        ->add(Crud::PAGE_INDEX, Action::DETAIL);
  }

  public function configureCrud(Crud $crud): Crud
  {
    return $crud
      // the labels used to refer to this entity in titles, buttons, etc.
      ->setEntityLabelInSingular('Utilisateur')
      ->setEntityLabelInPlural('Utilisateurs');



    // the Symfony Security permission needed to manage the entity
    // (none by default, so you can manage all instances of the entity)
    // ->setEntityPermission('ROLE_ADMIN');
  }

  public function configureFields(string $pageName): iterable
  {


    return [
      FormField::addPanel('Informations principales'),
      EmailField::new('email', 'Adresse mail'),
      Field::new('password', 'Mot de passe')->setFormType(PasswordType::class)->onlyWhenCreating(),
      TextField::new('firstName', 'Prenom'),
      TextField::new('lastName', 'Nom'),
      TelephoneField::new('telephone', 'Téléphone'),
      ChoiceField::new('Roles', 'Roles')
        ->setChoices([
          'Administrateur' => 'ROLE_ADMIN',
          'Utilisateur' => 'ROLE_USER',
        ])->setFormTypeOptions([
          'multiple' => true,
        ]),

      FormField::addPanel('Residences'),
      AssociationField::new('residences', 'Habite a')->setFormTypeOptions(['by_reference' => false])->formatValue(function ($value, $entity) {
        $str = $entity->getResidences()[0];
        for ($i = 1; $i < $entity->getResidences()->count(); $i++) {
          $str = $str . ", " . $entity->getResidences()[$i];
        }
        return $str;
      }),
      TextField::new('nbAppartement', 'Numéro d\'appartement'),

    ];
  }
}
