<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class Mailer
{
    private $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendTestMessage(User $user)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('parking@vroumvroum.com', 'Parking Vroum Vroum'))
            ->to(new Address('tom.siatka@laposte.net', $user->getFirstName()))
            ->subject('Merci pour votre location')
            ->htmlTemplate('email/test.html.twig')
            ->context([
                'user' => $user,
            ]);

        $this->mailer->send($email);
    }
}
