<?php

namespace App\DataFixtures;

use App\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class PlaceFixtures extends Fixture implements DependentFixtureInterface
{
    public const PLACE_REF = 'place-ref';

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 51; $i++) {
            $place = new Place();
            $place->setNumber($i);
            $place->setIsUsed(false);
            $place->setParking($this->getReference(ParkingFixtures::PARKING_REF));
            $manager->persist($place);
        }

        $placeRef = new Place();
        $placeRef->setNumber(123);
        $placeRef->setIsUsed(true);
        $placeRef->setParking($this->getReference(ParkingFixtures::PARKING_REF));
        $manager->persist($placeRef);
        $placeRef->addUser($this->getReference(MichelFixtures::MICHEL_REF));
        $this->addReference(self::PLACE_REF, $placeRef);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ParkingFixtures::class,
            MichelFixtures::class
        ];
    }
}
